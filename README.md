# noplayerdrag

## Description
A little module to disable player dragging of tokens forcing them to use the WASD/arrow keys to move a token.

Settings support disable always or disable in combat.

## Installation
Install from the foundry install module tab or paste the following link
[module.json](https://gitlab.com/tposney/noplayerdrag/raw/main/module.json)

## Usage
Once installed and activated, use the config options to choose the config you want. Reload is not required when changing options.


## Contributing
Contributions welcome.

## License
MIT License. Do whatever you want with the code.
